# DrupalOrg DrupalCon Theme

Drupal theme used for DrupalCon events (hosted on https://events.drupal.org).

## DrupalCon logo / wordmark

This theme was created to be used in the https://events.drupal.org site. If
you want to use it in a project, note that you cannot use the DrupalCon logo
or wordmark for anything that is not an official Drupal Association
sanctioned event.

## Compile assets

Ensure that you are on version 20 of node: `node -v`.
If you are not, use [nvm](https://github.com/nvm-sh/nvm#installing-and-updating) to
change it like this: `nvm use 20` (you might need to `nvm install 20` first).

Then:
* `nvm use 20`
* `npm install`
* `npm run gulp`
