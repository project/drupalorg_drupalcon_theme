(function ($) {
  Drupal.behaviors.schedule = {
    attach: function (context, settings) {
      
      if ($('.view-drupalcon-sessions.view-display-id-block_1:not(.page-node-schedule-processed)').length === 1) {
        $('.view-drupalcon-sessions.view-display-id-block_1').addClass('page-node-schedule-processed');

        var $scheduleFilters = $('.view-drupalcon-sessions .nav-filters');

        $scheduleFilters.find(':checkbox').change(function() {

          // countFilters();
          activeFilters();

        });

        $('.nav-button').click(function (e) {
          var target = $(this).attr('aria-controls');
          $(this).attr('aria-expanded', $(this).attr('aria-expanded')==='true'?'false':'true' );
          $('#' + target).toggleClass('h-open');

          $('.nav-button').not(this).each(function(){
            $(this).attr('aria-expanded', 'false');
          });

          $('.nav-content:not(#' + target + ')').removeClass('h-open');

          fixedNavST.refresh();

        });

        // Active day label.
        if ($('.nav-days .is-active').length === 1) {
          var activeDay = $('.nav-days .is-active').html();
          $('.view-nav .button-days .button-label').html(activeDay);
        }

        // Sticky utils.
        var headerHeight = $('.h-header-active').outerHeight();

        var fixedNavST = ScrollTrigger.create({
          trigger: '.view-nav',
          start: 'top top+=' + headerHeight,
          endTrigger: 'footer',
          pin: '.view-nav',
          pinSpacing: false,
          toggleClass: 'h-pinned'
        });

        // Count active filters.
        function countFilters() {
          var filterActive = $(':checkbox:checked').length;
          if (filterActive > 0) {
            $('.filters-num').html('(' + filterActive + ')');
          } else {
            $('.filters-num').html('');
          }
        }
        countFilters();

        // Add class to wrapper if active filters.
        function activeFilters() {
          var $filterWrapper = $('.form-checkboxes');
          var selectedFilters = $(':checkbox:checked').length;

          if(selectedFilters > 0) {
            $filterWrapper.addClass('h-filtered');
          } else {
            $filterWrapper.removeClass('h-filtered');
          }

        }
        activeFilters();


        // Pinned timeslot.
        const timeslots = gsap.utils.toArray('.view-grouping .group-title');
        var navHeight = $('.view-nav-bar').outerHeight() + headerHeight;

        timeslots.forEach(timeslot => {
          var timeslotHeight = timeslot.offsetHeight;
          ScrollTrigger.create({
            trigger: timeslot,
            pin: true,
            start: 'top top+=' + navHeight,
            endTrigger: $(timeslot).next(),
            end: "bottom-=10 top+=" + (navHeight + timeslotHeight),
            pinSpacing: false
          });

        });

      }



    }
  };
}(jQuery));
