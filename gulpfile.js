// include gulp & configuration
var gulp = require('gulp');
var config = require('./gulp_config.json');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');

// include plug-ins
var $ = require('gulp-load-plugins')({
  rename: {
    'gulp-sass-bulk-import': 'bulkSass'
  }
});

// Watch.
gulp.task('watch', function() {
  gulp.watch(config.scss.files, gulp.series('scss'));
  // gulp.watch(config.images.files, gulp.series('images'));
  gulp.watch(config.scripts.files, gulp.series('scripts'));
});

// Sass.
gulp.task('scss', function() {
  return gulp.src(config.scss.files)
  .pipe($.bulkSass())
  .pipe($.sourcemaps.init())
  .pipe(sass({
    includePaths: ['./node_modules/breakpoint-sass/stylesheets']
  }).on('error', $.notify.onError({
    message: "File: <%= error.message %>",
    title: "SCSS task / Error"
  })))
  .pipe(autoprefixer('last 10 versions'))
  .pipe($.cssnano())
  .pipe($.sourcemaps.write('./maps'))
  .pipe(gulp.dest(config.scss.dest));
});

// JS.
gulp.task('scripts', function() {
  return gulp.src(config.scripts.files)
  .pipe($.sourcemaps.init())
  .pipe($.concat('main.min.js'))
  .pipe($.terser().on('error', $.notify.onError({
    message: "File: <%= error.message %>",
    title: "Scripts task / Error"
  })))
  .pipe($.sourcemaps.write('maps'))
  .pipe(gulp.dest(config.scripts.dest));
});

// JSON.
// gulp.task('json', function() {
//   return gulp.src(config.json.files)
//   .pipe($.sourcemaps.init())
//   .pipe($.sourcemaps.write('maps'))
//   .pipe(gulp.dest(config.json.dest));
// });

// Utils.
// gulp.task('vendors', function() {
//   return gulp.src(config.vendors.files)
//   .pipe($.concat('vendors.min.js'))
//   .pipe($.uglify())
//   .pipe(gulp.dest(config.vendors.dest));
// });

// Images.
// gulp.task('images', function() {
//   return gulp.src(config.images.files)
//   .pipe(imagemin([
//     imagemin.svgo({
//       plugins: [
//         {removeViewBox: false},
//         {cleanupIDs: false}
//       ]
//     })
//   ], {verbose: true}))
//   .pipe(gulp.dest(config.images.dest));
// });

gulp.task('default', gulp.series('scss', 'scripts'));
